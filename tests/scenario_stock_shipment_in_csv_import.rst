=====================================
Stock Shipment In Csv Import Scenario
=====================================

Imports::

    >>> from proteus import Model, Wizard
    >>> from decimal import Decimal
    >>> import datetime
    >>> import os
    >>> today = datetime.date.today()
    >>> from trytond.modules.stock_shipment_in_csv_import.tests.tools import read_csv_file
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> from trytond.tests.tools import activate_modules


Install stock_shipment_in_csv_import::

    >>> config = activate_modules('stock_shipment_in_csv_import')

Create company::

    >>> _ = create_company()
    >>> company = get_company()

Create Supplier::

    >>> Party = Model.get('party.party')
    >>> supplier = Party(name='Supplier 1')
    >>> supplier.save()

Create Warehouse::

    >>> Location = Model.get('stock.location')
    >>> warehouse_loc, = Location.find([('code', '=', 'WH')])
    >>> warehouse_loc.name = 'Warehouse 1'
    >>> warehouse_loc.save()

Create Product::

    >>> ProductUom = Model.get('product.uom')
    >>> unit, = ProductUom.find([('name', '=', 'Unit')])
    >>> ProductTemplate = Model.get('product.template')
    >>> Product = Model.get('product.product')
    >>> product = Product()
    >>> template = ProductTemplate()
    >>> template.name = 'Product 1'
    >>> template.default_uom = unit
    >>> template.type = 'goods'
    >>> template.purchasable = True
    >>> template.list_price = Decimal('10')
    >>> template.cost_price = Decimal('5')
    >>> template.cost_price_method = 'fixed'
    >>> template.save()
    >>> product.template = template
    >>> product.save()

Get stock locations::

    >>> Location = Model.get('stock.location')
    >>> lost_found_loc, = Location.find([('type', '=', 'lost_found')])
    >>> storage_loc, = Location.find([('code', '=', 'STO')])
    >>> internal_loc = Location(name='Internal', type='storage')
    >>> internal_loc.save()

Create Move::

    >>> Move = Model.get('stock.move')
    >>> move = Move()
    >>> move.product = product
    >>> move.quantity = 1
    >>> move.from_location = internal_loc
    >>> move.to_location = storage_loc
    >>> move.currency = company.currency
    >>> move.save()

Create Shipment In::

    >>> ShipmentIn = Model.get('stock.shipment.in')
    >>> shipment_in = ShipmentIn()
    >>> shipment_in.effective_date = today
    >>> shipment_in.supplier = supplier
    >>> shipment_in.warehouse = warehouse_loc
    >>> shipment_in.reference = 'Reference 1'
    >>> shipment_in.company = company
    >>> shipment_in.moves.append(move)
    >>> shipment_in.save()
    >>> shipment_in_waiting, = ShipmentIn.duplicate([shipment_in])
    >>> shipment_in_waiting.reference = 'Reference 2'
    >>> shipment_in_waiting.save()
    >>> shipment_in_waiting.click('receive')

Create Configuration::

    >>> Configuration = Model.get('stock.configuration')
    >>> config = Configuration()
    >>> config.csv_headers = "'reference','supplier','contact_address','warehouse','effective_date','incoming_moves.product','incoming_moves.quantity'"
    >>> config.save()

Execute Wizard::

    >>> csv_import = Wizard('stock.shipment.in.csv_import')
    >>> filename = os.path.join(os.path.dirname(__file__), 'csv_test.csv')
    >>> csv_import.form.csv_file = read_csv_file(filename)
    >>> csv_import.execute('import_')
    >>> shipment_in.effective_date
    datetime.date(2016, 9, 21)
    >>> len(Move.find())
    5
    >>> csv_import = Wizard('stock.shipment.in.csv_import')
    >>> filename = os.path.join(os.path.dirname(__file__), 'csv_test.csv')
    >>> csv_import.form.csv_file = read_csv_file(filename)
    >>> csv_import.execute('import_')
    >>> len(Move.find())
    5

When product doesn't exist::

    >>> csv_import = Wizard('stock.shipment.in.csv_import')
    >>> filename = os.path.join(os.path.dirname(__file__), 'csv_test_error.csv')
    >>> csv_import.form.csv_file = read_csv_file(filename)
    >>> csv_import.execute('import_') # doctest: +IGNORE_EXCEPTION_DETAIL
    Traceback (most recent call last):
        ...
    UserError: ('UserError', ('Product "Product 2" not found.', ''))

